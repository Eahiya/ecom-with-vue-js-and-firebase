import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Admin from '../views/Admin.vue'
import Overview from '../views/Overview.vue'
import Products from '../views/Products'
import { db } from '../database/firebase'
Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
    {
    path: '/admin',
    name: 'admin',
    component: Admin,
    meta: { requiresAuth: true },
    children: [
      {
        path:'overview',
        name: 'overview',
        component: Overview,
      },
      {
        path: 'products',
        name: 'products',
        component: Products
      }
    ]
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})


router.beforeEach((to, from, next) => {
  const currentUser = db.auth().currentUser // here catch current user from database
  const requiresAuth = to.matched.some((recode) =>recode.meta.requiresAuth) // here matched is store every route.. then you can access or implement any logic
    // here check current route's have a meta, requiresAuth.. if present it will return true or undefine);
    
  if(requiresAuth && !currentUser){
    next('/');
  }else if(requiresAuth && currentUser){
    next()
  }else{
    next()
  }

}) 

export default router
