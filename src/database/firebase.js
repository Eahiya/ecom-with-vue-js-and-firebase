import firebase, { database } from 'firebase'
import "firebase/firestore";
import 'firebase/storage';
// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyAUfp-IiBkj54qdCQo8N8Gj31cJ2a0q6Uk",
    authDomain: "leafy-future-254503.firebaseapp.com",
    databaseURL: "https://leafy-future-254503.firebaseio.com",
    projectId: "leafy-future-254503",
    storageBucket: "leafy-future-254503.appspot.com",
    messagingSenderId: "884563310125",
    appId: "1:884563310125:web:2df3b4589e373b059caaa4",
    measurementId: "G-K87DY1G33S"
  };
  // Initialize Firebase
 const db = firebase.initializeApp(firebaseConfig);
 const firebaseDB = firebase.firestore();


export {db, firebaseDB}