import Vue from 'vue'
import App from './App.vue'
import router from './router'

import jQuery from 'jquery'
window.$ = window.jQuery = jQuery;

import 'popper.js';
import 'bootstrap';
import './assets/css/app.scss';
import { db } from './database/firebase';


import VueFirestore from 'vue-firestore'
Vue.use(VueFirestore, {
  key: 'id',         // the name of the property. Default is '.key'.
  enumerable: true  //  whether it is enumerable or not. Default is true.
})


import Swal from 'sweetalert2'
window.Swal = Swal
const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true,
  onOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer)
    toast.addEventListener('mouseleave', Swal.resumeTimer)
  }
})
window.Toast = Toast

Vue.component('NaveBar', require('./components/NaveBar').default)

Vue.config.productionTip = false


let app = '';
db.auth().onAuthStateChanged(function(user) {
  if(!app){ // it is alawys true:  if user login or not not a big dell
    new Vue({
      router,
      render: h => h(App)
    }).$mount('#app')
  }
  console.log(user);
  
});


